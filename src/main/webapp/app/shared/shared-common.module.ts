import { NgModule } from '@angular/core';

import { Project1SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [Project1SharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [Project1SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Project1SharedCommonModule {}
